!pip install --upgrade gspread_dataframe
!pip install --upgrade gspread
import gspread
import requests
from time import time
from gspread_dataframe import set_with_dataframe
import pandas as pd

def load_df_to_gsheets(data_frame, doc, title, force=False, formats=[], index=False, drop_data=False):
      # https://gspread.readthedocs.io/en/latest/user-guide.html
      gc = gspread.service_account(filename='/content/drive/MyDrive/crd/python_to_gs.json') # reports@voltaic-cirrus-267318.iam.gserviceaccount.com
      sh = gc.open_by_key(doc)
      
      # create if not exists
      counter = 0
      done = True
      while True:
          print('iter'+str(counter))
          try:
              if len(list(filter(lambda w: w.title == title, sh.worksheets()))) == 0:
                  sh.add_worksheet(title=title, rows="1", cols="1")

              if force:
                  if len(list(filter(lambda w: w.title == title, sh.worksheets()))) > 0:
                      worksheet = sh.worksheet(title)
                      sh.del_worksheet(worksheet)

                  worksheet = sh.add_worksheet(title=title, rows="1", cols="1")

              worksheet = sh.worksheet(title)
              
              if drop_data:
                  worksheet.resize(rows=1)
              
              set_with_dataframe(worksheet, data_frame, include_index=index)
              worksheet.format('1:1', {
                  'textFormat': {'bold': True},
                  'horizontalAlignment': 'CENTER',
                  'verticalAlignment': 'MIDDLE',
                  'wrapStrategy': 'WRAP'
              })
          except requests.exceptions.ReadTimeout:
              print("reconnecting...")
              time.sleep(3)
          counter+=1
          print(counter)
          if (counter == 3)|(done == True):
              if (counter == 3):
                  print('ReadTimeoutError')
              else:
                  print('done')
              break    
          
      if len(formats) > 0:
          for f in formats:
              worksheet.format(f['range'], f['value'])